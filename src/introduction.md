# Introduction

> **Warning:** `micro-tower` is in early development stages and is not feature
> complete yet. This framework may require some basic knowledge about `async`
> in rust and the [tower] crate.

`micro-tower` is a framework to easily generate and manage services.

This framework will allow you to generate a service from a function signature
and function body. A service declaration should look like:

```rust,ignore
#[micro_tower::codegen::service]
async fn custom_service(req: Request) -> Response {
    todo!()
}
```

Fore more details on how to use this framework see [Getting Started](./getting_started.md)

`micro-tower` is based on the [tower] middleware (as the name implies). So it is
possible to use the generated services with `tower::ServiceBuilder` and other
utilities from the [tower] crate.

## Repository

[![Pipeline Status](https://gitlab.com/robert-oleynik/micro-tower/badges/main/pipeline.svg)](https://gitlab.com/robert-oleynik/micro-tower/-/pipelines)

- [GitLab](https://gitlab.com/robert-oleynik/micro-tower)

<!-- TODO: crates.io, docs.rs -->

## Main Dependencies

- [tower] for the services
- [tokio] as the async runtime
- [serde] and [serde_json] for parsing messages
- [tracing] for logging

## Versioning

The framework follows [Semantic Versioning 2.0.0](https:://semver.org)

[tower]: https://crates.io/crates/tower
[tokio]: https://crates.io/crates/tokio
[serde]: https://crates.io/crates/serde
[serde_json]: https://crates.io/crates/serde_json
[tracing]: https://crates.io/crates/tracing
