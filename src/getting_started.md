# Getting Started

## Installation

To install this framework you can simply add `micro_tower` to your `Cargo.toml`
file:

```toml
[dependencies]
micro-tower = "~0.1.0-rc.1"
```

## Creating Your First Service

You first service can be created by putting the proc-attribute
`#[micro_tower::codegen::service]` in front of your function declaration. The
resulting code should like:

```rust,ignore
#[micro_tower::codegen::service]
async fn first_service(request: Request) -> Response {
    todo!("put your code here")
}
```

As you may have noticed: The service signature contains one input of type `Request`
and one output type `Response`. Both are placeholder for your request and response
type and can be replaced by any type.

As a result of that a simple service returning `"Hello World"` when called,
looks like:

```rust,ignore
#[micro_tower::codegen::service]
async fn first_service(_request: ()) -> &'static str {
    "Hello World"
}
```

Another example of an service adding one to your request can look like:

```rust
#[micro_tower::codegen::service]
async fn first_service(req: i32) -> i32 {
    req + 1
}
```

## Starting Your Service

> **Not implemented yet**

`todo!()`
